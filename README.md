## Getting Started (Mac/Linux)
Install python first.
You need to install numpy, scipy and scikit-learn before starting with the course.
You can do that by running following :
``` 
pip install -U scikit-learn
pip install -U numpy
pip install -U scipy
```

If you do not have pip, install it as it is a very common way of installying python packages.


## Video tutorial links : 
### Tutorial 1
https://www.youtube.com/watch?v=cKxRvEZd3Mw

Is Code for this lesson Published : Yes

### Tutorial 2
https://www.youtube.com/watch?v=tNa99PG8hR8

Is Code for this lesson Published : No